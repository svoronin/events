package com.vlashel.event.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.vlashel.event.resource.EventResource;
import com.vlashel.event.service.DaoEventService;
import com.vlashel.event.service.MongoDaoEventServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;


/**
 * @author vshel
 */
public class EventManagementApplication extends Application<EventManagementConfiguration> {

    public static void main(String[] args) throws Exception {
        args = new String[2];
        args[0] = "server";
        args[1] = EventManagementApplication.class.getResource("/config.yml").getPath();
        new EventManagementApplication().run(args);
    }

    @Override
    public void run(EventManagementConfiguration configuration, Environment environment) {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(DaoEventService.class).to(MongoDaoEventServiceImpl.class).in(Singleton.class);
            }
        });
        EventResource eventResource = injector.getInstance(EventResource.class);
        environment.jersey().register(eventResource);
    }

    @Override
    public String getName() {
        return "Json Filter";
    }

}
