package com.vlashel.event.api;

/**
 * @author vshel
 */
public enum EventType {
    INTERACTION,
    QUESTION,
    ANSWER,
    /** for verifying a connection or a topic. */
    PING
}
