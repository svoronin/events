package com.vlashel.event.api;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Predicate;

import javax.annotation.Nullable;

/**
 * @author vshel
 */
public class Product {
    //Checks for products with no external id
    public static final Predicate<Product> HAS_EXTERNAL_ID = new Predicate<Product>() {
        @Override
        public boolean apply(@Nullable Product input) {
            return input != null && input.getExternalId() != null;
        }
    };
    private String externalId;
    private String name;
    private Double price;
    private String imageUrl;

    @JsonProperty
    // @JsonSerialize (using = LowerCaseSerializer.class)
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty
    // @JsonDeserialize (using = LowerCaseDeserializer.class)
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public Double getPrice() {
        return price;
    }

    @JsonProperty
    public void setPrice(Double price) {
        this.price = price;
    }

    @JsonProperty
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Product withExternalId(final String externalId) {
        this.externalId = externalId;
        return this;
    }

    public Product withName(final String name) {
        this.name = name;
        return this;
    }

    public Product withPrice(final Double price) {
        this.price = price;
        return this;
    }

    public Product withImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Product product = (Product) o;

        if (externalId != null ? !externalId.equals(product.externalId) : product.externalId != null) {
            return false;
        }
        if (imageUrl != null ? !imageUrl.equals(product.imageUrl) : product.imageUrl != null) {
            return false;
        }
        if (name != null ? !name.equals(product.name) : product.name != null) {
            return false;
        }
        if (price != null ? !price.equals(product.price) : product.price != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = externalId != null ? externalId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        return result;
    }
}
