package com.vlashel.event.api;

/**
 * @author vshel
 */
public interface HasClient {
    public String getClient();
    public void setClient(String client);
}
