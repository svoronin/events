package com.vlashel.event.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Function;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;
import org.mongojack.ObjectId;

import javax.annotation.Nullable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author vshel
 */
public class Event implements HasClient {
    public static final String ID_FIELD = "_id";
    public static final String EVENT_TYPE_FIELD = "eventType";
    public static final String CLIENT_FIELD = "client";
    public static final String USER_ID_FIELD = "userId";
    public static final String EMAIL_ADDRESS_FIELD = "emailAddress";
    public static final String PRR_ENCRYPTED_EMAIL_ADDRESS_FIELD = "prrEncryptedEmailAddress";
    public static final String EMAIL_ADDRESS_HASH_FIELD = EMAIL_ADDRESS_FIELD + ".hashedString";
    public static final String EVENT_TIME_FIELD = "eventTime";
    public static final String IS_VALID_FIELD = "isValid";
    public static final String COORDINATE_FIELD = "coordinate";
    public static final String VERSION_FIELD = "version";
    public static final String PRODUCT_EXTERNAL_ID_FIELD = "products.externalId";
    public static final String LOCALE_FIELD = "locale";

    private String id;
    private EventType eventType;
    private String emailAddress;
    private String plaintextEmailAddress;
    // temporary storage
    private String prrEncryptedEmailAddress;
    private String client;
    private DateTime eventTime;
    private Set<Product> products;
    private String userId;
    private String userName;
    private String locale;
    private String coordinate;
    private Boolean isValid = Boolean.TRUE;
    private Long version;

    public Event withIsValid(final Boolean isValid) {
        this.isValid = isValid;
        return this;
    }

    @JsonProperty(IS_VALID_FIELD)
    public Boolean getIsValid() {
        return isValid;
    }

    @JsonProperty(IS_VALID_FIELD)
    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Event withLocale(final String locale) {
        this.locale = locale;
        return this;
    }

    public Event withUserName(final String userName) {
        this.userName = userName;
        return this;
    }

    public Event withEventType(final EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public Event withUserId(final String userId) {
        this.userId = userId;
        return this;
    }

    public Event withProducts(final Set<Product> products) {
        this.products = products;
        return this;
    }

    public Event withId(final String id) {
        this.id = id;
        return this;
    }

    public Event withEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public Event withPlaintextEmailAddress(final String plaintextEmailAddress) {
        this.plaintextEmailAddress = plaintextEmailAddress;
        return this;
    }

    public Event withClient(final String client) {
        this.client = client;
        return this;
    }

    public Event withEventTime(final DateTime eventTime) {
        this.eventTime = eventTime;
        return this;
    }

    public Event withCoordinate(final String coordinate) {
        this.coordinate = coordinate;
        return this;
    }

    public Event withVersion(final Long version) {
        this.version = version;
        return this;
    }

    @ObjectId
    @JsonProperty(ID_FIELD)
    public String getId() {
        return id;
    }

    @ObjectId
    @JsonProperty(ID_FIELD)
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty(EVENT_TYPE_FIELD)
    public EventType getEventType() {
        return eventType;
    }

    @JsonProperty(EVENT_TYPE_FIELD)
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @JsonProperty(EMAIL_ADDRESS_FIELD)
    /** @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn */
    public String getEmailAddress() {
        return emailAddress;
    }

    @JsonProperty(EMAIL_ADDRESS_FIELD)
    /** @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @JsonIgnore // don't serialize by default
    /** @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn */
    public String getEmailAddressHash() {
        return (emailAddress == null) ? null : emailAddress/*.getHashedString()*/;
    }

    @JsonIgnore // don't serialize by default
    public String getPlaintextEmailAddress() {
        return plaintextEmailAddress;
    }

    @JsonIgnore // don't serialize by default
    public void setPlaintextEmailAddress(String plaintextEmailAddress) {
        this.plaintextEmailAddress = plaintextEmailAddress;
    }

    @JsonIgnore
    /** @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn */
    public String getPrrEncryptedEmailAddress() {
        return prrEncryptedEmailAddress;
    }

    @JsonIgnore
    /** @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn */
    public void setPrrEncryptedEmailAddress(String emailAddress) {
        //the deserializer this method's been annotated with will decrypt the email address
        prrEncryptedEmailAddress = emailAddress;
    }

    @JsonProperty(CLIENT_FIELD)
    // @JsonSerialize (using = LowerCaseSerializer.class)
    public String getClient() {
        return client;
    }

    @JsonProperty(CLIENT_FIELD)
    // @JsonDeserialize (using = LowerCaseDeserializer.class)
    public void setClient(String client) {
        this.client = client;
    }

    @JsonProperty(EVENT_TIME_FIELD)
    public DateTime getEventTime() {
        return eventTime;
    }

    @JsonProperty(EVENT_TIME_FIELD)
    public void setEventTime(DateTime eventTime) {
        this.eventTime = eventTime;
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public Set<Product> getProducts() {
        return products;
    }

    @JsonDeserialize(as = LinkedHashSet.class)
    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public String getCoordinate() {
        return coordinate;
    }

    @JsonProperty(COORDINATE_FIELD)
    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public Long getVersion() {
        return version;
    }

    @JsonProperty(VERSION_FIELD)
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Event event = (Event) o;
        return new EqualsBuilder()
                .append(client, event.client)
                .append(emailAddress, event.emailAddress)
                .append(plaintextEmailAddress, event.plaintextEmailAddress)
                .append(eventTime, event.eventTime)
                .append(eventType, event.eventType)
                .append(id, event.id)
                .append(locale, event.locale)
                .append(products, event.products)
                .append(userId, event.userId)
                .append(userName, event.userName)
                .append(coordinate, event.coordinate)
                .append(version, event.version)
                .append(isValid, event.isValid)
                .append(prrEncryptedEmailAddress, event.prrEncryptedEmailAddress)
                .isEquals();
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder(11, 47)
                .append(id)
                .append(eventType)
                .append(emailAddress)
                .append(plaintextEmailAddress)
                .append(client)
                .append(eventTime)
                .append(products)
                .append(userId)
                .append(userName)
                .append(locale)
                .append(coordinate)
                .append(version)
                .append(isValid)
                .append(prrEncryptedEmailAddress)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("eventType", eventType)
                .append("emailAddress", emailAddress)
                .append("plaintextEmailAddress", plaintextEmailAddress)
                .append("prrEncryptedEmailAddress", prrEncryptedEmailAddress)
                .append("client", client)
                .append("eventTime", eventTime)
                .append("products", products)
                .append("userId", userId)
                .append("userName", userName)
                .append("locale", locale)
                .append("isValid", isValid)
                .append("coordinate", coordinate)
                .append("version", version).toString();
    }

    public static final Function<Event, String> GET_ID = new Function<Event, String>() {
        @Nullable
        @Override
        public String apply(@Nullable Event input) {
            return input != null ? input.getId() : null;
        }
    };

    /**
     * stick emailAddress into plaintextEmailAddress (because it gets serialized over REST) in place.
     *
     * @return the same Event object
     * @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn
     */
    public Event revealPlaintext() {
        return withPlaintextEmailAddress(getEmailAddress());
    }

    /**
     * sticks emailAddress into plaintextEmailAddress because it gets serialized over REST.
     * mutates the input event!!!
     *
     * @see com.bazaarvoice.rolodex.jackson.EventJsonMixIn
     */
    public static final Function<Event, Event> MODIFY_SHOW_PLAINTEXT = new Function<Event, Event>() {
        @Override
        public Event apply(Event input) {
            return input.revealPlaintext();
        }
    };
}
