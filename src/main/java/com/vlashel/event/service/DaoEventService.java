package com.vlashel.event.service;

import com.vlashel.event.api.Event;

import java.util.List;

/**
 * @author vshel
 */
public interface DaoEventService {
    String save(Event event);
    void update(Event event);
    void delete(String id);
    List<Event> list();
    List<Event> findByClient(String client);
    Event findById(String id);
}
