package com.vlashel.event.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.inject.Singleton;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.vlashel.event.api.Event;
import org.mongojack.DBCursor;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;
import org.mongojack.internal.MongoJackModule;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vshel
 */
@Singleton
public class MongoDaoEventServiceImpl implements DaoEventService {

    private JacksonDBCollection<Event, String> db;

    public MongoDaoEventServiceImpl() throws UnknownHostException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        MongoJackModule.configure(mapper);
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB mongoDB = mongoClient.getDB("mydb");
        DBCollection dbCollection = mongoDB.getCollection("testData");
        db = JacksonDBCollection.wrap(dbCollection, Event.class, String.class, mapper);
    }

    @Override
    public String save(Event event) {
        WriteResult<Event, String> result = db.insert(event);
        return result.getSavedId();
    }

    @Override
    public void update(Event event) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public List<Event> list() {
        List<Event> events = new ArrayList<>();
        DBCursor<Event> cursor = db.find();
        for (Event e : cursor) {
            events.add(e);
        }
        return events;
    }

    @Override
    public Event findById(String id) {
        return db.findOneById(id);
    }

    @Override
    public List<Event> findByClient(String client) {
        List<Event> events = new ArrayList<>();
        DBCursor<Event> cursor = db.find(DBQuery.is("client", client));
        for (Event e : cursor) {
            events.add(e);
        }
        return events;
    }
}
