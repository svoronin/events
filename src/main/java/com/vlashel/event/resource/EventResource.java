package com.vlashel.event.resource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.google.inject.Inject;
import com.vlashel.event.api.Event;
import com.vlashel.event.service.DaoEventService;
import com.vlashel.filter.JsonFilterService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author vshel
 */
@Path("event")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EventResource {
    @Inject
    private DaoEventService daoEventService;
    @Inject
    private JsonFilterService jsonFilterService;

    @POST
    @Path("save")
    public Response insert(Event event) {
        String id = daoEventService.save(event);

        ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
        TextNode textNode = JsonNodeFactory.instance.textNode(id);
        objectNode.set("id", textNode);

        return Response.status(Response.Status.ACCEPTED).entity(objectNode).build();
    }

    @GET
    @Path("find/{id}")
    public Response getEventById(@PathParam("id") String id) {
        Event event = daoEventService.findById(id);
        return Response.status(Response.Status.FOUND).entity(event).build();
    }

    @GET
    @Path("find")
    public Response getEventsByClient(@QueryParam("client") String client, @QueryParam("path") List<String> paths) {
        List<Event> events = daoEventService.findByClient(client);
        if (paths.isEmpty()) {
            return Response.status(Response.Status.FOUND).entity(events).build();
        } else {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.convertValue(events, JsonNode.class);
            JsonNode result = jsonFilterService.filter(paths, node);
            return Response.status(Response.Status.FOUND).entity(result).build();
        }
    }
}
